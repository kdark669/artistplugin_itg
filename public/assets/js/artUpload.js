document.addEventListener('DOMContentLoaded', function (e) {
    let uploadForm = document.getElementById('uploadArt');
    uploadForm.addEventListener('submit', (e) =>{
        e.preventDefault();
        let activeFlag = true;
        if(!e.target['artistImage'].files[0]){
            var eartist_file = document.getElementById('eartist_file');
            eartist_file.innerHTML = 'Art is Required';
            eartist_file.style.display = 'block';
            activeFlag = false;
        }
        if(!e.target['name'].value){
            var ename = document.getElementById('ename');
            ename.innerHTML = 'Name is Required';
            ename.style.display = 'block';
            activeFlag = false;
        }
        if(!e.target['description'].value){
            var edescription = document.getElementById('edescription');
            edescription.innerHTML = 'Description is Required';
            edescription.style.display = 'block';
            activeFlag = false;
        }
        if(!e.target['stock'].value){
            var estock = document.getElementById('estock');
            estock.innerHTML = 'Stock is Required';
            estock.style.display = 'block';
            activeFlag = false;
        }

        if(!e.target['regular_price'].value){
            var eregular_price = document.getElementById('eregular_price');
            eregular_price.innerHTML = 'Regular Price is Required';
            eregular_price.style.display = 'block';
            activeFlag = false;
        }
        if(!e.target['sales_price'].value){
            var esales_price = document.getElementById('esales_price');
            esales_price.innerHTML = 'Sales Price is Required';
            esales_price.style.display = 'block';
            activeFlag = false;
        }
        let data = {
        };
        data =  new FormData();
        data.append('artist_file', e.target['artistImage'].files[0]);
        data.append('artist_id', e.target['artistId'].value);
        data.append('stock', e.target['stock'].value);
        data.append('name', e.target['name'].value);
        data.append('description', e.target['description'].value);
        data.append('regular_price', e.target['regular_price'].value);
        data.append('sales_price', e.target['sales_price'].value);
        data.append('length', e.target['length'].value);
        data.append('width', e.target['width'].value);
        data.append('height', e.target['height'].value);

        let url = window.location.href+'';
        if (activeFlag) {
            axios.post(url, data ,{
                headers: {
                    'content-type': 'multipart/form-data' // do not forget this
                },
                params:{
                    'custom':'uploadArt'
                }
            })
                .then(res => {
                    if(res){
                        e.target['artistImage'].value = '';
                        e.target['stock'].value = '';
                        e.target['name'].value = '';
                        e.target['description'].value = '';
                        e.target['regular_price'].value ='';
                        e.target['sales_price'].value ='';
                        e.target['length'].value ='';
                        e.target['width'].value ='';
                        e.target['height'].value ='';
                        var success = document.getElementById('updated');
                        success.innerHTML = '<span>Art Uploaded</span>';
                        success.style.right = 0;
                        success.style.marginRight = '5px';
                        setTimeout(function () {
                            var success = document.getElementById('updated');
                            success.style.display = 'none';
                        }, 2000);
                    }
                }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Something went wrong</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })
        }
    })
});