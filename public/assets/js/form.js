document.addEventListener('DOMContentLoaded', function (e) {
    let registerForm = document.getElementById('form');
    registerForm.addEventListener('submit', (e) => {
        e.preventDefault();
        let activeFlag = true;
        let data = {
            user_login: e.target['user_login'].value,
            first_name: e.target['first_name'].value,
            last_name: e.target['last_name'].value,
            user_email: e.target['user_email'].value,
            user_pass: e.target['user_pass'].value,
            user_repass: e.target['user_repass'].value,

        };
        if (!data.user_login) {
            var euser_login = document.getElementById('euser_login');
            euser_login.innerHTML = 'UserName is Required';
            euser_login.style.display = 'block';
            activeFlag = false;
        }
        if (!data.first_name) {
            var efirst_name = document.getElementById('efirst_name');
            efirst_name.innerHTML = 'FirstName is Required';
            efirst_name.style.display = 'block';
            activeFlag = false;
        }
        if (!data.last_name) {
            var elast_name = document.getElementById('elast_name');
            elast_name.innerHTML = 'LastName is Required';
            elast_name.style.display = 'block';
            activeFlag = false;
        }
        if (!data.user_email) {
            var euser_email = document.getElementById('euser_email');
            euser_email.innerHTML = 'Email is Required';
            euser_email.style.display = 'block';
            activeFlag = false;
        }
        if (!data.user_pass) {
            var euser_pass = document.getElementById('euser_pass');
            euser_pass.innerHTML = 'Password is Required';
            euser_pass.style.display = 'block';
            activeFlag = false;
        }
        if (!data.user_repass) {
            var euser_repass = document.getElementById('euser_repass');
            euser_repass.innerHTML = 'Confirm Password  is Required';
            euser_repass.style.display = 'block';
            activeFlag = false;
        }

        if (data.user_pass !== data.user_repass) {
            var errorPassword = document.getElementById('errorPassword');
            errorPassword.innerText = 'Password and Confirm Password are not Same'
        }


        let url = window.location.href+ '';
        if (activeFlag) {
            axios.post(
                url, data,{
                    params:{
                        'register':'registerArtist'
                    }
                }
            ).then(res => {
                if (res) {
                    console.log('i am in');
                    e.target['user_login'].value = '';
                    e.target['first_name'].value = '';
                    e.target['last_name'].value = '';
                    e.target['user_email'].value = '';
                    e.target['user_pass'].value = '';
                    e.target['user_repass'].value = '';
                    var success = document.getElementById('updated');
                    success.innerHTML = '<span>Welcome To BellyMonk. Please Login</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    setTimeout(function () {
                        var success = document.getElementById('updated');
                        success.style.display = 'none';
                    }, 2000);
                }
                else {
                    console.log('i am off');
                    var errorR = document.getElementById('errorR');
                    errorR.innerHTML = '<span>Fail to Create Artist</span>';
                    errorR.style.right = 0;
                    errorR.style.marginRight = '5px';
                    setTimeout(function () {
                        var errorR = document.getElementById('errorR');
                        errorR.style.display = 'none';
                    }, 2000);
                }
            }).catch(error => {
                console.log('i am off ko bau');
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Internal working error</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })
        }
    })
});