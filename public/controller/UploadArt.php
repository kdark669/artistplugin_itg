<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('UploadArt')):
class UploadArt
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle(){

            if ($_SERVER["REQUEST_METHOD"] == "POST"  && !isset($_GET['register'])&& !isset($_GET['moveArt']) && isset($_GET['custom']) && $_GET['custom'] == "uploadArt") {
                $data = json_decode(file_get_contents('php://input'), 1);
                $file = $_FILES['artist_file']["tmp_name"];
                $artistId = sanitize_text_field($_POST['artist_id']);
                $stock = sanitize_text_field($_POST['stock']);
                $artname = sanitize_text_field($_POST['name']);
                $description = sanitize_text_field($_POST['description']);
                $regular_price = sanitize_text_field($_POST['regular_price']);
                $sales_price = sanitize_text_field($_POST['sales_price']);
                $length = sanitize_text_field($_POST['length']);
                $width = sanitize_text_field($_POST['width']);
                $height = sanitize_text_field($_POST['height']);
                global $wpdb;
                $table = $wpdb->prefix . 'artist_itg';
                $check = getimagesize($file);
                if ($check !== false) {
                    $name =$_FILES['artist_file']["name"];
                    $plugin_dir = ABSPATH . 'wp-content/plugins/Artist/';
                    $target_dir = $plugin_dir . '/Uploads/';
                    $target_file = $target_dir . basename($_FILES["artist_file"]["name"]);
                    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                    $extensions_arr = array("jpg", "jpeg", "png", "gif");
                    if (in_array($imageFileType, $extensions_arr)) {

                        $data = array(
                            'artist_id' => $artistId,
                            'image' => $name,
                            'name' =>$artname,
                            'description' =>$description,
                            'stock' =>$stock,
                            'regular_price'=> $regular_price,
                            'sales_price'=>$sales_price,
                            'length'=>$length,
                            'width'=>$width,
                            'height'=>$height,
                            'created' => date("Y-m-d H:i:s"),
                        );
                        move_uploaded_file($file, $target_dir . $name);
                        $success = $wpdb->insert($table, $data);
                        if ($success) {
                            echo json_encode(array('res' => true));
                            exit;
                        } else {
                            echo json_encode(array('res' => false));
                            exit;
                        }
                    }

                }
                die();
            }
    }
}

new UploadArt();
endif;