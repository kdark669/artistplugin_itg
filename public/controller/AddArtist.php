<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('AddArtist')):
class AddArtist
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }

    public static function handle()
    {

        if($_SERVER['REQUEST_METHOD'] == 'POST'  && !isset($_GET['custom']) && !isset($_GET['moveArt']) && isset($_GET['register'])){
        	$data = json_decode(file_get_contents('php://input'), 1);
            require_once(ABSPATH . WPINC . '/registration.php');
            $new_user_id = array(
                    'user_login'		=> sanitize_text_field($data['user_login']),
                    'user_pass'	 		=> sanitize_text_field($data['user_pass']),
                    'user_email'		=> sanitize_text_field($data['user_email']),
                    'first_name'		=> sanitize_text_field($data['first_name']),
                    'last_name'			=> sanitize_text_field($data['last_name']),
                    'user_registered'	=> date('Y-m-d H:i:s'),
                    'role'				=> 'artist'
            );
            $exists_email = email_exists( $new_user_id['user_email'] );
            $exists_username = username_exists(  $new_user_id['user_login'] );

            if ( $exists_email || $exists_username) {
                echo json_encode(array('res' => false, 'message' => __('Something went wrong. Please try again later.')));
                die();
            }else{
                $user_id = wp_insert_user( $new_user_id ) ;
                wp_new_user_notification($user_id);
	            $subject = 'Account Created ';
	            $message = 'Hi There! Welcome To Bellymonk. We just wanted to let you know your user account on Bellymonk.com as Artist has been approved.';
	            $to = $new_user_id['user_email'];
				wp_mail( $to, $subject, $message );
	            echo json_encode(array('res' => true, 'message' => __('New User has been inserted.')));
            }
        }

//            if($new_user_id) {
//                // send an email to the admin alerting them of the registration
//                wp_new_user_notification($new_user_id);
//                // log the new user in
////                wp_setcookie($data['user_login'], $data['user_pass'], true);
////                wp_set_current_user($new_user_id, $data['user_login']);
////                do_action('wp_login', $data['user_login']);
//                // send the newly created user to the home page after logging them in
//                wp_redirect(home_url());
//                echo json_encode(array('res' => true, 'message' => __('New row has been inserted.')));
//               exit;
//            }
//            else {
//                echo json_encode(array('res' => false, 'message' => __('Something went wrong. Please try again later.')));
//                exit;
//            }
//            die();
//    }
    }

    public static function add_login_Link(){
        $query_res = get_page_by_title('Artists')->ID;
        if(!is_user_logged_in()) {
            ?>
            <a href="http://bellymonk.test/?page_id=<?php echo $query_res?>">
               Become An Artist
            </a>
            <?php
        }
    }
}
new AddArtist();
endif;
