<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('ArtistInfo')):
class ArtistInfo
{
	public static function our_artist_info(){
        global $product;
        $p_id = $product->id;
        $post = get_post($p_id);
        $user_detail = get_userdata($post->post_author);
        $role = $user_detail->roles[0];
        $artistId = $user_detail->ID;
	    $query_res = get_page_by_title('Artist Details')->ID;
        if($role == 'artist'){
        ?>
        <div class="large-3 columns artprofilelink">
                <span class="some-talent">
                 <span>Art By: </span>
                <a href="http://bellymonk.test/?page_id=<?php echo $query_res?>&aId=<?php echo $artistId?>">
                    <?php echo $user_detail->user_nicename ?>
                </a>
                </span>
        </div>
        <?php
        }
    }
}
endif;