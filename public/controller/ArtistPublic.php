<?php

if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('ArtistPublic')):
class ArtistPublic
{
    public static function createPage( ) {
        global $user_ID;
        $new_post = array(
            'post_title' => 'Artists',
            'post_content' => 'Artists',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_type' => 'page',
            'post_name' =>'artist',
        );
        $detail_page = [
            'post_title' => 'Artist Details',
            'post_content' => 'Detail',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => $user_ID,
            'post_type' => 'page',
            'post_name' =>'artist detail',
        ];
        if(get_page_by_title( 'Artist Details', 'page', 'page' ) == NULL){
            $post_id = wp_insert_post($detail_page);
            if( !$post_id )
                wp_die('Error creating template page');
            else
                update_post_meta( $post_id, '_wp_page_template', 'artistDetail.php' );
        }

        if(get_page_by_title( 'Artists', 'page', 'page') == NULL ){
            $post_id = wp_insert_post($new_post);
            if( !$post_id )
                wp_die('Error creating template page');
            else
                update_post_meta( $post_id, '_wp_page_template', 'artist.php' );
        }

    }

    // Page template filter callback
    public static function catch_plugin_template($template) {
        if( is_page_template('artist.php') ) {
            wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js', array(), false, false);
            if (!is_user_logged_in()) {
                wp_enqueue_style('form', plugins_url() . '/Artist/public/assets/css/style.css');
                wp_enqueue_script('form', plugins_url() . '/Artist/public/assets/js/form.js', array(), false, true);
                $template = WP_PLUGIN_DIR . '/Artist/templates/artist.php';
                return $template;
            }else{
                wp_enqueue_style('artistStyle', plugins_url() . '/Artist/public/assets/css/artistStyle.css');
                wp_enqueue_script('uploadForm', plugins_url() . '/Artist/public/assets/js/artUpload.js', array(), false, true);
                $template = WP_PLUGIN_DIR . '/Artist/templates/artistProfile.php';
                return $template;
            }
        }

        elseif(is_page_template('artistDetail.php')){
            wp_enqueue_style('artistStyle', plugins_url() . '/Artist/public/assets/css/artistStyle.css');
            $template = WP_PLUGIN_DIR . '/Artist/templates/artistDetail.php';
            return $template;
        }
    }

    public static function createRole(){
        add_role(
            'artist',
            'Artist',
            array(
                'read'         => true,
                'delete_posts' => false
            )
        );
    }


}
endif;