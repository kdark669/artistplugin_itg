<?php

require_once ('controller/ArtistPublic.php');
require_once ('controller/AddArtist.php');
require_once ('controller/UploadArt.php');
require_once ('controller/ArtistInfo.php');

add_filter('page_template', array('ArtistPublic','catch_plugin_template'));
add_filter('page_template', array('ArtistPublic','catch_plugin_template'));
add_action( 'woocommerce_after_add_to_cart_button', array('ArtistInfo','our_artist_info' ));
add_action('wp_head',array('AddArtist','add_login_Link'));

