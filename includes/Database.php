<?php

if(!defined('ABSPATH')){
    die();
}
if(!class_exists('Database')):
class Database
{
    public function getTableName()
    {
        global $wpdb;
        return $wpdb->prefix . 'artist_itg';
    }
    public static function createDatabaseTable() {
        global $wpdb;
        $table_name = self::getTableName();
        $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE $table_name(
				id int(11) NOT NULL AUTO_INCREMENT,
				artist_id bigint(11) unsigned NOT NULL,
				image varchar(100) NOT NULL,
				name varchar(100) NOT NULL,
				description varchar(500) NOT NULL,
				stock varchar(100) NOT NULL,
				regular_price varchar(100) NOT NULL,
				sales_price varchar(100) NOT NULL,
				length varchar(100),
				width varchar(100),
				height varchar(100),
				accepted_flag tinyint default 0, 
                created datetime NOT NULL,
				PRIMARY KEY  (id),
				FOREIGN KEY (artist_id) REFERENCES wp_users(`id`)
				) $charset_collate";
        $wpdb->query($sql);
    }

    public function removeArtistDatabase(){
        global $wpdb;
        $table_name      = self::getTableName();
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
    }

}
endif;