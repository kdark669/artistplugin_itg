document.addEventListener('DOMContentLoaded', function (e) {
    let rejectArt = document.getElementsByClassName('rejectArt');
    Array.from(rejectArt).forEach(function (element) {
        element.addEventListener('submit',(e)=>{
            e.preventDefault();
            let data = {
                'artist_Id':e.target['imageId'].value,
                'user_id':e.target['user_id'].value,
            };
            let url = window.location.href+'';
            console.log(url);
            axios.post(url, data,{
                    params:{
                        'custom':'rejectArt'
                    }
                }
                ).then(res => {
                if(res){
                    var success = document.getElementById('updated');
                    success.innerHTML = '<span>Art Uploaded</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    var adminaction = element.parentElement.className;
                    console.log(adminaction.innerHTML = '<span>Accepted</span>');
                    adminaction.innerHTML = '<span>Accepted</span>';
                    setTimeout(function () {
                        var success = document.getElementById('updated');
                        success.style.display = 'none';
                    }, 2000);
                }

            }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Something went wrong</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })
        })
    });
});