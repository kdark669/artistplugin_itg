document.addEventListener('DOMContentLoaded', function (e) {
    let moveArt = document.getElementsByClassName('moveArt');
    Array.from(moveArt).forEach(function (element) {
        element.addEventListener('submit', (e)=> {
            e.preventDefault();
            let data = {
                'artist_file': e.target['artistImage'].value,
                'artist_id': e.target['artistId'].value,
                'user_id': e.target['user_Id'].value,
                'stock':e.target['stock'].value,
                'name':e.target['name'].value,
                'description':e.target['description'].value,
                'regular_price':e.target['regular_price'].value,
                'sales_price':e.target['sales_price'].value,
                'length':e.target['length'].value,
                'width':e.target['width'].value,
                'height':e.target['height'].value,
            };
            let url = window.location.href+'';
            axios.post(url, data,{
                params:{
                    'moveArt':'moveArt'
                }
            }).then(res => {
                if(res){
                    e.target['artistImage'].value = '';
                    var success = document.getElementById('updated');
                    success.innerHTML = '<span>Art Uploaded</span>';
                    success.style.right = 0;
                    success.style.marginRight = '5px';
                    var adminaction = element.parentElement.className;
                    console.log(adminaction.innerHTML = '<span>Accepted</span>');
                    adminaction.innerHTML = '<span>Accepted</span>';
                    setTimeout(function () {
                        var success = document.getElementById('updated');
                        success.style.display = 'none';
                    }, 2000);
                }
            }).catch(error => {
                var errorR = document.getElementById('errorR');
                errorR.innerHTML = '<span>Something went wrong</span>';
                errorR.style.right = 0;
                errorR.style.marginRight = '5px';
                setTimeout(function () {
                    var errorR = document.getElementById('errorR');
                    errorR.style.display = 'none';
                }, 2000);
            })

        })
    })
});