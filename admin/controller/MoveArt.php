
<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('MoveArt')):
    class MoveArt
    {
        public function __construct()
        {
            add_action('init', array($this, 'handle'));
        }

        public static function handle()
        {

            if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_GET['custom']) && isset($_GET['moveArt']) && !isset($_GET['register'])){
                global $wpdb;
                $table = $wpdb->prefix . 'artist_itg';
                $url = site_url();
                $parent_post_id = 0;
                $data = json_decode(file_get_contents('php://input'), 1);
	            $artistId = sanitize_text_field($data['artist_id']);
                $stock = sanitize_text_field($data['stock']);
                $name = sanitize_text_field($data['name']);
                $description = sanitize_text_field($data['description']);
                $regular_price = sanitize_text_field($data['regular_price']);
                $sales_price = sanitize_text_field($data['sales_price']);
                $length = sanitize_text_field($data['length']);
                $width = sanitize_text_field($data['width']);
                $height = sanitize_text_field($data['height']);
                $user_id = sanitize_text_field($data['user_id']);
                $file = $url . sanitize_text_field($data['artist_file']);
                $filename = basename($file);
                $upload_file = wp_upload_bits($filename, null, file_get_contents($file));
                $image_url = $upload_file['url'];
                $user_email = get_userdata( $user_id )->user_email;
                if (!$upload_file['error']) {
                    $wp_filetype = wp_check_filetype($filename, null);
                    $post = array(
                        'post_author' => $user_id,
                        'post_mime_type' => $wp_filetype['type'],
                        'post_parent' => $parent_post_id,
                        'post_title' => $name,
                        'post_content' => '<img class="alignnone size-medium wp-image-30" src="'.$image_url.'" alt="" width="300" height="175" />',
                        'post_status' => 'publish',
                        'post_type' =>'product'
                    );
                    $post_id = wp_insert_post( $post,'');
                    if($post_id){
                        $attach_id = get_post_meta($post_id, '_thumbnail_id', true);
                        add_post_meta($post_id, '_thumbnail_id', $attach_id);
                        wp_set_object_terms( $post_id, 'Artist', 'product_cat' );
                        wp_set_object_terms($post_id, 'simple', 'product_type');

                        update_post_meta( $post_id, '_visibility', 'visible' );
                        update_post_meta( $post_id, '_stock_status', 'instock');
                        update_post_meta( $post_id, 'total_sales', '0');
                        update_post_meta( $post_id, '_downloadable', 'no');
                        update_post_meta( $post_id, '_virtual', 'yes');
                        update_post_meta( $post_id, '_regular_price', $regular_price );
                        update_post_meta( $post_id, '_sale_price', $sales_price );
                        update_post_meta( $post_id, '_purchase_note', "" );
                        update_post_meta( $post_id, '_featured', "no" );
                        update_post_meta( $post_id, '_weight', "" );
                        update_post_meta( $post_id, '_length', $length );
                        update_post_meta( $post_id, '_width', $width );
                        update_post_meta( $post_id, '_height', $height );
                        update_post_meta($post_id, '_sku', "");
                        update_post_meta( $post_id, '_product_attributes', array());
                        update_post_meta( $post_id, '_sale_price_dates_from', "" );
                        update_post_meta( $post_id, '_sale_price_dates_to', "" );
                        update_post_meta( $post_id, '_price', $sales_price );
                        update_post_meta( $post_id, '_sold_individually', "" );
                        update_post_meta( $post_id, '_manage_stock', "no" );
                        update_post_meta( $post_id, '_backorders', "no" );
                        update_post_meta( $post_id, '_stock', $stock );
                        update_post_meta( $post_id, '_product_image_gallery',$image_url);

                        $upload_dir = wp_upload_dir();
                        $image_data = file_get_contents($image_url);
                        $filename = basename($image_url);

                        if (wp_mkdir_p($upload_dir['path']))
                            $file = $upload_dir['path'] . '/' . $filename;
                        else
                            $file = $upload_dir['basedir'] . '/' . $filename;
                        file_put_contents($file, $image_data);
                        $attachment = array(
                            'post_author' => $user_id,
                            'post_mime_type' => $wp_filetype['type'],
                            'post_title' => sanitize_file_name($filename),
                            'post_content' => '<img class="alignnone size-medium wp-image-30" src="'.$image_url.'" alt="" width="300" height="175" />',
                            'post_status' => 'inherit'
                        );
                        require_once(ABSPATH . 'wp-admin/includes/image.php');
                        $attach_id = wp_insert_attachment($attachment, $file, $post_id);
                        $attach_data = wp_generate_attachment_metadata($attach_id, $file);
                        $res1 = wp_update_attachment_metadata($attach_id, $attach_data);
                        $res2 = set_post_thumbnail($post_id, $attach_id);

                        $data = array(
                            'accepted_flag' => 1
                        );
                        $success = $wpdb->update($table, $data,array('id'=>$artistId));
                        if($success){
	                        $subject = 'Art Approved';
	                        $message = 'Hi There! We just wanted to let you know your Art '.$image_url.' has been accepted and placed to product';
	                        $to = $user_email;
	                        wp_mail( $to, $subject, $message );
                            echo json_encode(array('res' =>'true'));
                            exit;
                        }
                        else{
                            echo json_encode(array('res' =>'false'));
                            exit;
                        }

                    }



                }
            }
        }

    }

    new MoveArt();
endif;