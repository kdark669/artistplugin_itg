<?php

require_once ('artistMenu.php');
if(!class_exists('ArtistAdmin')):
class ArtistAdmin
{
    public static function createArtistMenu(){
        add_menu_page(
            'Artists',
            'Artists',
            'administrator',
            'itg_artist',
            array('artistMenu', 'artistMenuCallback'),
            'dashicons-art'
        );
        add_submenu_page(
            'itg_artist',
            'Arts',
            'Arts',
            'administrator',
            'itg_artist_arts',
            array('artistMenu', 'artistArtsMenuCallback')
        );
    }

}
endif;