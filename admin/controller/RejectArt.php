<?php
if (!defined('ABSPATH')) {
    die;
}
if (!class_exists('RejectArt')):

class RejectArt
{
    public function __construct()
    {
        add_action( 'init', array( $this, 'handle' ) );
    }
    public static function handle(){

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['custom']) &&$_GET['custom'] == 'rejectArt') {
            global $wpdb;
            $table = $wpdb->prefix . 'artist_itg';

	        $data = json_decode(file_get_contents('php://input'), 1);
            $artistId = sanitize_text_field($data['artist_Id']);
	        $user_id = sanitize_text_field($data['user_id']);
	        $user_email = get_userdata( $user_id )->user_email;
            $success = $wpdb->delete( $table, array( 'id' => $artistId ) );
            if($success){
                echo json_encode(array('res' => true));
                exit;
            }
            else{
                echo json_encode(array('res' => false));
                exit;
            }
        }
    }

}
new RejectArt();
endif;