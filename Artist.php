<?php


/**
 * Plugin Name: itgArtist
 * Description: its demo
 * Version: 1.0.0
 * Author: ITG-Rajeev Rajchal
 * Author URI:
 * Text Domain:
 * Domain Path: languages
 *
 * License: GPLv2 or later
 * Domain Path: languages
 *
 */
if (!defined('ABSPATH')) {
    die('We\'re sorry, but you can not directly access this file.');
}
if(!class_exists('Artist')):
class Artist
{
    private static $instance = null;

    private function __construct() {
        $this->initializeHooks();
        $this->setupDatabase();
        $this->removeDatabase();
        $this->createPages();
        $this->createRoles();
    }

    public static function getInstance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function initializeHooks() {
        if ( is_admin() ) {
            require_once( 'admin/admin.php' );
        }
        require_once( 'public/public.php' );

    }

    private function setupDatabase() {
        require_once ('includes/Database.php');
        register_activation_hook( __FILE__, array('Database','createDatabaseTable' ) );
    }
    private function removeDatabase() {
        require_once ('includes/Database.php');
        register_deactivation_hook( __FILE__, array('Database','removeArtistDatabase' ) );
    }

    private function createPages(){
        register_activation_hook( __FILE__, array('ArtistPublic','createPage' ) );
        register_activation_hook( __FILE__, array('ArtistDetail','createPage' ) );
    }

    private function createRoles(){
        register_activation_hook( __FILE__, array('ArtistPublic','createRole' ) );
    }

}
endif;
Artist::getInstance();