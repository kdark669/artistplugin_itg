<?php
get_header();
$user_id = $_GET['aId'];

if(isset($user_id)) {
	$avatar_url = get_avatar_url( $user_id );
	$user       = get_userdata( $user_id );
	$attach     = $wpdb->get_results( " SELECT * FROM wp_posts where wp_posts.post_author = $user_id and post_type = 'attachment' " );
	?>
	<div class="container-fluid">
		<div class="profile-card">
			<div class="banner">
				<img src="wp-content/plugins/Artist/public/assets/img/banner.png" class="banner-img">
			</div>
			<div class="profile-info">
				<div class="profile-pic">
					<img src="<?php echo $avatar_url ?>">
				</div>
				<div class="artist-info">
					<p class="artist-name"><?php echo $user->display_name ?></p>
					<p class="artist-name"><?php echo $user->nickname ?></p>
					<p class="artist-bio">ARTIST, ENTREPRENEUR, DREAMER</p>
				</div>
			</div>
		</div>
		<div class="artist-collection">
			<p class="artist-collection-title"><?php echo strtoupper( $user->display_name ) ?>'S COLLECTIONS</p>

			<div class="artist-collection-items">
				<?php
				if ( empty( $attach ) ) {
					?>
					<p>The Art Collection is Empty</p>
					<?php
				} else {
					foreach ( $attach as $a ) {
//                        ?>
                        <p> <?php echo $a->post_content ?></p>

                        <?php


					}
				}
				?>
			</div>
		</div>
	</div>

	<?php
}else{
    ?>
    <p>hello world</p>
    <?php
}
get_footer();
