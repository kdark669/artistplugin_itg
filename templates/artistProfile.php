<?php
get_header();
$users = wp_get_current_user();
$user_role = ( array )$users->roles;
$avatar_url = get_avatar_url($users->ID);
$post = get_post($users->ID);
$user_id = get_current_user_id();
$attach = $wpdb->get_results(" SELECT * FROM wp_posts where wp_posts.post_author = $user_id and post_type = 'attachment' ");
?>
    <!--errors-->
    <div class="updated notice success" id="updated">
    </div>
    <div class="errorR notice" id="errorR">
    </div>
<?php

if ($user_role[0] == 'artist') {
    ?>
    <div class="container-fluid">
        <div class="profile-card">
            <div class="banner">
                <img src="wp-content/plugins/Artist/public/assets/img/banner.png" class="banner-img">
            </div>
            <div class="profile-info">
                <div class="profile-pic">
                    <img src="<?php echo $avatar_url ?>">
                </div>
                <div class="artist-info">
                    <p class="artist-name"><?php echo $users->display_name ?></p>
                    <p class="artist-bio">ARTIST, ENTREPRENEUR, DREAMER</p>
                </div>
            </div>
        </div>

        <div class="Art">
            <form class="uploadArt" id="uploadArt" method="post" enctype="multipart/form-data">
                <input type="hidden" name="artistId" id="artistId" value="<?php echo get_current_user_id(); ?>">
                <div class="form-group">
                    <input type="file" name="artistImage" id="artistImage" placeholder="Artist Image">
                    <span class="error" id="eartist_file"></span>
                </div>
                <div class="form-group">
                    <input type="text" name="name" id="name" placeholder="Art Name">
                    <span class="error" id="ename"></span>
                </div>
                <div class="form-group">
                    <textarea type="number" name="description" id="description" placeholder="Description"></textarea>
                    <span class="error" id="edescription"></span>
                </div>
                <div class="form-group">
                    <input type="number" name="stock" id="stock" placeholder="stock">
                    <span class="error" id="estock"></span>
                </div>
                <div class="form-group">
                    <input type="text" name="regular_price" id="regular_price" placeholder="regular price">
                    <span class="error" id="eregular_price"></span>
                </div>
                <div class="form-group">
                    <input type="text" name="sales_price" id="sales_price" placeholder="sales price">
                    <span class="error" id="esales_price"></span>
                </div>
                <div class="form-group">
                    <input type="text" name="length" id="length" placeholder="Art Length">
                    <span class="error" id="elength"></span>
                </div>
                <div class="form-group">
                    <input type="text" name="width" id="width" placeholder="Art Width">
                    <span class="error" id="ewidth"></span>
                </div>

                <div class="form-group">
                    <input type="text" name="height" id="height" placeholder="Art Height">
                    <span class="error" id="eheight"></span>
                </div>
                <button type="submit">Upload Art</button>
            </form>
        </div>
        <div class="artist-collection">
            <p class="artist-collection-title">YOUR COLLECTIONS</p>
            <div class="artist-collection-items">
                <?php
                if(empty($attach)){
                    ?>
                    <p>Your Collection is Empty</p>
                    <?php
                }
                foreach ( $attach as $a ) {
//                        ?>
                    <p> <?php echo $a->post_content ?></p>
                    <?php
                }

                ?>

            </div>

        </div>
    </div>
    <?php
}
else{
    ?>
    <div class="onlyAdmin">
        <p>Would You Like To Be An Artist</p>
        <form action="" method="post">
            <button type='submit' name="beArtist" class="btn"> Yes</button>
            <?php
            if(isset($_POST['beArtist'])){
                $u = new WP_User( $user_id );
                $u->remove_role( 'customer' );
                $u->add_role( 'artist' );
                echo "<script type='text/javascript'>
                window.location=document.location.href;
                </script>";
            }
            ?>
        </form>
    </div>
    <?php
}
get_footer();