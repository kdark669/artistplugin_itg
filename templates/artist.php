<?php
get_header();
?>
    <div class="updated notice success" id="updated">
    </div>
    <div class="errorR notice" id="errorR">
    </div>
    <div class="container-fluid">
        <div class="register-container">
            <div class="register-banner">
                <img src="wp-content/plugins/Artist/public/assets/img/Beapart.png" class="img-fluid" alt="BE A PART OF OUR TEAM">
            </div>
            <div class="register-form">
                <form class="needs-validation form" novalidate="novalidate" action="" id="form" method="post">
                    <div class="form-group">
                        <input type="text" name="user_login" id="user_login"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="Username" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="euser_login"></small>
                    </div>
                    <div class="form-group">
                        <input type="text" name="first_name" id="first_name"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="First Name" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="efirst_name"></small>
                    </div>
                    <div class="form-group">
                        <input type="text" name="last_name" id="last_name"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="Last Name" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="elast_name"></small>
                    </div>
                    <div class="form-group">
                        <input type="text" name="user_email" id="user_email"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="Your Email" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="euser_email"></small>
                    </div>
                    <div class="errorPassword" id="errorPassword">

                    </div>
                    <div class="form-group">
                        <input type="password" name="user_pass" id="user_pass"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="Your Password" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="euser_pass"></small>
                    </div>

                    <div class="form-group">
                        <input type="password" name="user_repass" id="user_repass"
                               class="form-control-lg border border-dark font-weight-bold"
                               aria-describedby="usernameHelp" placeholder="Confirm Password" required="required">
                        <small class="invalid-feedback error form-text text-dark font-weight-bold"
                               id="euser_repass"></small>
                    </div>

                    <button type="submit" name="btnsave btn-lg btn-dark font-weight-bold btn-block shadow-none"
                            id="btnsave">Sign Up
                    </button>

                </form>
            </div>
        </div>
        <div class="gallery">
            <!--Gallery Title-->
            <div class="gallery-head">
                <span>SHARE THE LOVE</span>
                <span>#BELLYMONK</span>
            </div>

            <hr>

            <!--Gallery Images-->
            <div class="gallery-contents">
                <div class="gallery-image">
                    <img src="wp-content/plugins/Artist/public/assets/img/dp.png" alt="">
                </div>
                <div class="gallery-image">
                    <img src="wp-content/plugins/Artist/public/assets/img/art1.png" alt="">
                </div>
                <div class="gallery-image">
                    <img src="wp-content/plugins/Artist/public/assets/img/dp.png" alt="">
                </div>
                <div class="gallery-image">
                    <img src="wp-content/plugins/Artist/public/assets/img/art1.png" alt="">
                </div>
                <div class="gallery-image">
                    <img src="wp-content/plugins/Artist/public/assets/img/art1.png" alt="">
                </div>
            </div>

            <!--Gallery Buttons-->
            <div class="gallery-btns text-center">
                <button type="submit" class="btn-lg btn-dark shadow-none mr-1">Load More...</button>
                <button type="submit" class="btn-lg btn-dark shadow-none"><i class="fa fa-instagram"></i>&nbsp;Follow on
                    Instagram
                </button>

            </div>
        </div>
    </div>

<?php
get_footer();