<?php
global $wpdb;
$table_name = $wpdb->prefix . 'artist_itg';
$results = $wpdb->get_results("SELECT * FROM $table_name");
if (empty($results)) {
    ?>
    <p>No Any Artist Uploaded The Arts </p>
    <?php

}
else {
    ?>
    <div class="artContainer">
        <div class="updated notice success" id="updated">
        </div>
        <div class="errorR notice" id="errorR">
        </div>
        <?php
        foreach ($results as $row) {
            $image = $row->image;
            $plugin_dir = '/wp-content/plugins/Artist/Uploads/';
            $artist_detail = get_userdata($row->artist_id);
            ?>

            <div class="uploadedArt">
                <p><span style="font-weight: bold;">Uploaded By: </span><?php echo $artist_detail->user_nicename ?></p>
                <p><span style="font-weight: bold;">ArtistId: </span><?php echo $artist_detail->ID ?></p>
                <img class="upload-images" src="<?php echo $plugin_dir . $image ?>"
                     alt="artist image">
                <?php if ($row->accepted_flag == 0) { ?>
                    <div class="adminAction">
                        <form action="" id="moveArt" class="moveArt" method="post"
                              enctype="multipart/form-data">
                            <input type="text" name="ImageId" value="<?php echo $row->id ?>" hidden>
                            <input type="text" name="artistId" value="<?php echo $row->id ?>" hidden>
                            <input type="text" name="user_Id" value="<?php echo $artist_detail->ID ?>" hidden>
                            <input type="text" name="name" value="<?php echo $row->name ?>" >
                            <input type="text" name="description" value="<?php echo $row->description ?>" >
                            <input type="text" name="stock" value="<?php echo $row->stock ?>" >
                            <input type="text" name="regular_price" value="<?php echo $row->regular_price ?>" >
                            <input type="text" name="sales_price" value="<?php echo $row->sales_price ?>" >
                            <input type="text" name="length" value="<?php echo $row->length ?>" >
                            <input type="text" name="width" value="<?php echo $row->width  ?>" >
                            <input type="text" name="height" value="<?php echo $row->height  ?>" >
                            <input type="text" name="artistImage" value="<?php echo $plugin_dir . $image ?>"
                                   hidden>
                            <button type="submit" class="btn-small" name="accept">Accept<Button>
                        </form>


                        <form action="" method="post" id="rejectArt" class="rejectArt" >
                            <input type="text" name="imageId" value="<?php echo $row->id ?>" hidden>
                            <input type="text" name="user_id" value="<?php echo $artist_detail->ID ?>" hidden>
                            <button type="submit" class="btn-small" name="reject">Reject<Button>
                        </form>
                    </div>
                <?php } else {?>

                    <div class="adminAction">
                        <span>Accepted</span>
                    </div>
                <?php } ?>
            </div>


            <?php

        } ?>
    </div>
    <?php

}
?>